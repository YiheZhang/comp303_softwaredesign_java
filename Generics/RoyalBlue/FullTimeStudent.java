/*
COMP 303 Assignment #3.1
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.ArrayList;

//Full time Student object extends an abstract Student object
//The constructor creates an Student object
class FullTimeStudent extends Student{
    public FullTimeStudent(String name, int studentID, ArrayList<Course> courseHistory, ArrayList<Grade> courseGradesHistory) {
        super.setName(name);
        super.setStudnetID(studentID);
        super.setCourseHistory(courseHistory);
        super.setCourseGradesHistory(courseGradesHistory);
        super.setIsFullTime(true);
    }
}