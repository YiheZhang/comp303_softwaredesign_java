/*
COMP 303 Assignment #3.1
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//grade object
//this object contains a pair of an Course object and an Grade object
public class Grade {
    private Course course;
    private String grade;

    public Grade(Course course, String grade) {
        this.course = course;
        this.grade = grade;
    }

    //methods to get private values
    public Course getCourse() {
        return this.course;
    }
    public String getGrade() {
        return this.grade;
    }

    //methods to change private values
    public void setCourse(Course course)
    {
        this.course = course;
    }
    public void setGrade(String grade) {
        this.grade = grade;
    }
}