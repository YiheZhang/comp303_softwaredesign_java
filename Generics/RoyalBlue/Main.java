/*
COMP 303 Assignment #3.1
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        //initialize temp variables
        ArrayList<Student> students = new ArrayList<>();
        ArrayList<Course> courseHistory = new ArrayList<>();
        ArrayList<Grade> courseGradesHistory = new ArrayList<>();

        Student student = null;
        Course course = null;
        String grade = null;
        String name = null;
        String courseName = null;
        String courseNumber = null;
        boolean isFullTime = true;
        int studentID = -1;

        //A loop for user i/o, let user select a number
        int endLoop = 0;
        while(endLoop == 0) {
            int selection = mainMenu();
            if (selection == 1) register(students, course);
            if (selection == 2) addGrade(student, course, grade);
            if (selection == 3) {
                Student newStudent = createAStudent(name, studentID, courseHistory, courseGradesHistory, isFullTime);
                students.add(newStudent);
            }
            if (selection == 4) {
                Course newCourse = createACourse(courseName, courseNumber);
            }
            if (selection == 5) endLoop = 1;
        }
    }

    //return an int, let user select an option.
    //(1) register students to a course, (2) add a grade to a student for a course
    //(3) create a student, (4) create a course, (5) quite program.
    private static int mainMenu() {
        int selection = 5;
        return selection;
    }

    //Option #1, input an array of Student, a Course
    private static void register(ArrayList<Student> students, Course course) {
    }

    //Option #2, input a Student, a Course, a String for grade
    private static void addGrade(Student student, Course course, String grade) {
    }

    //Option #3, input a String for name, an int for student ID, a Course for course History,
    //an array of Course for course grades History, a boolean to determine if the student
    //is full time student. Return a Student object
    private static Student createAStudent(String name, int studentID, ArrayList<Course> courseHistory, ArrayList<Grade> courseGradesHistory, Boolean isFullTime) {
        if (isFullTime == true) return new FullTimeStudent(name, studentID, courseHistory, courseGradesHistory);
        else return new PartTimeStudent(name, studentID, courseHistory, courseGradesHistory);
    }

    //Option #4, input a String for course name, return a Course object
    private static Course createACourse(String courseName, String courseNumber) {
        Course course = new Course(courseName, courseNumber);
        return course;
    }
}