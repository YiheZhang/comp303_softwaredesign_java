/*
COMP 303 Assignment #3.1
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//Course object
//pair a String to a String
public class Course {
    private String courseName;
    private String courseNumber;

    public Course(String courseName, String courseNumber) {
        this.courseName = courseName;
        this.courseNumber = courseNumber;
    }

    //methods to get private values
    public String getCourseName() {
        return this.courseName;
    }
    public String getCourseNumber() {
        return this.courseNumber;
    }

    //methods to change private values
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }
}