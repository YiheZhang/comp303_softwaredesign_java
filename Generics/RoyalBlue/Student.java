/*
COMP 303 Assignment #3.1
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.ArrayList;

//Abstract Student object
//it contains Student name, Student ID, whether is full time, course history and course grades history
public abstract class Student {
    private String name;
    private int studentID;
    private boolean fullTime;
    private ArrayList<Course> courseHistory;
    private ArrayList<Grade> courseGradesHistory;

    //These methods return the value contained in the private variables
    public ArrayList<Course> getCourseHistory() {
        return this.courseHistory;
    }
    public int getStudnetID() {
        return this.studentID;
    }
    public String getName() {
        return this.name;
    }
    public Boolean isFullTime() {
        return this.fullTime;
    }
    public ArrayList<Grade> getCourseGradesHistory() {
        return this.courseGradesHistory;
    }

    //These methods let user set/change private variables' value
    public void setCourseGradesHistory(ArrayList<Grade> courseGradesHistory) {
        this.courseGradesHistory = courseGradesHistory;
    }
    public void setCourseHistory(ArrayList<Course> courseHistory) {
        this.courseHistory = courseHistory;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setStudnetID(int studnetID) {
        this.studentID = studnetID;
    }
    public void setIsFullTime(Boolean fullTime) {
        this.fullTime = fullTime;
    }
}
