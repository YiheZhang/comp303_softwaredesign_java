/*
COMP 303 Assignment #3.3
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.*;

//a Part Time Student class that extends an Student object
public class PartTimeStudent<C, G, I, T, B> extends Student<C, G, I, T, B> {
    public PartTimeStudent(T name, I studentID, ArrayList<C> courseHistory, ArrayList<G> courseGradesHistory, B isFullTimeStudent) {
        super.setName(name);
        super.setID(studentID);
        super.setCHistory(courseHistory);
        super.setGHistory(courseGradesHistory);
        super.setIsTrue(isFullTimeStudent);
    }
}
