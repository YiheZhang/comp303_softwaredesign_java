/*
COMP 303 Assignment #3.3
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//An interface to pair C to T
public interface Pair<C, T> {
    //methods to get private values
    public C getC();
    public T getT();
    //methods to set private values
    public void setC(C c);
    public void setT(T t);
}
