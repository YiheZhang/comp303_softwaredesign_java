/*
COMP 303 Assignment #3.3
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//Course object
//pair a String to a String
public class Course<C, T> implements Pair<C, T> {
    private C courseName;
    private T courseNumber;

    public Course(C courseName, T courseNumber) {
        this.courseName = courseName;
        this.courseNumber = courseNumber;
    }

    //methods to get private values
    public C getC() {
        return this.courseName;
    }
    public T getT() {
        return this.courseNumber;
    }

    //methods to change private values
    public void setC(C courseName) {
        this.courseName = courseName;
    }
    public void setT(T courseNumber) {
        this.courseNumber = courseNumber;
    }
}