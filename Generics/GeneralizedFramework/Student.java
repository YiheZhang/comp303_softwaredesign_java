/*
COMP 303 Assignment #3.3
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.*;

//Student class implements Generalized Interface
//C is an Course object, G is an Grade object, I is an Integer object, T is String, B is Boolean
public abstract class Student<C, G, I, T, B> implements GeneralizedInterface<C, G, I, T, B> {
    private T name;
    private I studentID;
    private ArrayList<C> courseHistory;
    private ArrayList<G> courseGradesHistory;
    private B isFullTimeStudent;

    //These methods return the value contained in the private variables
    public ArrayList<C> getCHistory() {
        return this.courseHistory;
    }
    public I getID() {
        return this.studentID;
    }
    public T getName() {
        return this.name;
    }
    public ArrayList<G> getGHistory() {
        return this.courseGradesHistory;
    }
    public B isTrue() {
        return this.isFullTimeStudent;
    }

    //These methods let user set/change private variables' value
    public void setGHistory(ArrayList<G> courseGradesHistory) {
        this.courseGradesHistory = courseGradesHistory;
    }
    public void setCHistory(ArrayList<C> courseHistory) {
        this.courseHistory = courseHistory;
    }
    public void setName(T name) {
        this.name = name;
    }
    public void setID(I studnetID) {
        this.studentID = studnetID;
    }
    public void setIsTrue(B isFullTimeStudent) {
        this.isFullTimeStudent = isFullTimeStudent;
    }
}
