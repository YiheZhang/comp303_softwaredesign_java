/*
COMP 303 Assignment #3.3
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.ArrayList;

//the generalized interface
public interface GeneralizedInterface<C, G, I, T, B> {
    //get history stored in C
    public ArrayList<C> getCHistory();

    //get ID stored in I
    public I getID();

    //get A's name
    public T getName();

    //get history stored in G
    public ArrayList<G> getGHistory();

    //get IsTrue
    public B isTrue();

    //set G's history
    public void setGHistory(ArrayList<G> g);

    //set C's History
    public void setCHistory(ArrayList<C> c);

    //Set A's name
    public void setName(T name);

    //Set A's ID (stored in I)
    public void setID(I id);

    //Set is True
    public void setIsTrue(B b);
}