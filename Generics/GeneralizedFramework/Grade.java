/*
COMP 303 Assignment #3.3
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//Grade object, it implements Object C and Object T
//where C is an Course object, T is an String object
public class Grade<C, T> implements Pair<C, T>{
    private C course;
    private T grade;
    public Grade(C course, T grade) {
        this.course = course;
        this.grade = grade;
    }

    //methods to get private values
    public C getC() {
        return this.course;
    }
    public T getT() {
        return this.grade;
    }

    //methods to set private values
    public void setC(C course) {
        this.course = course;
    }
    public void setT(T grade) {
        this.grade = grade;
    }
}