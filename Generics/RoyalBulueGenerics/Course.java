/*
COMP 303 Assignment #3.2
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//Course object
//Pair a String to a String
public class Course<C, T> {
    private C courseName;
    private T courseNumber;

    public Course(C courseName, T courseNumber) {
        this.courseName = courseName;
        this.courseNumber = courseNumber;
    }

    //methods to get private values
    public C getCourseName() {
        return this.courseName;
    }
    public T getCourseNumber() {
        return this.courseNumber;
    }

    //methods to change private values
    public void setCourseName(C courseName) {
        this.courseName = courseName;
    }
    public void setCourseNumber(T courseNumber) {
        this.courseNumber = courseNumber;
    }
}