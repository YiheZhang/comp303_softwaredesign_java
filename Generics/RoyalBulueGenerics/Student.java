/*
COMP 303 Assignment #3.2
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.*;

//Abstract Student object
//it contains Student name, Student ID, whether is full time, course history and course grades history
public abstract class Student<C, G, I, T, B> {
    private T name;
    private I studentID;
    private B fullTime;
    private ArrayList<C> courseHistory;
    private ArrayList<G> courseGradesHistory;

    //These methods return the value contained in the private variables
    public ArrayList<C> getCourseHistory() {
        return this.courseHistory;
    }
    public I getStudnetID() {
        return this.studentID;
    }
    public T getName() {
        return this.name;
    }
    public ArrayList<G> getCourseGradesHistory() {
        return this.courseGradesHistory;
    }
    public B isFullTime() {
        return this.fullTime;
    }

    //These methods let user set/change private variables' value
    public void setCourseGradesHistory(ArrayList<G> courseGradesHistory) {
        this.courseGradesHistory = courseGradesHistory;
    }
    public void setCourseHistory(ArrayList<C> courseHistory) {
        this.courseHistory = courseHistory;
    }
    public void setName(T name) {
        this.name = name;
    }
    public void setStudnetID(I studnetID) {
        this.studentID = studnetID;
    }
    public void setIsFullTime(B fullTime) {
        this.fullTime = fullTime;
    }
}