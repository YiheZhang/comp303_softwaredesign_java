/*
COMP 303 Assignment #3.2
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.*;

//Part time Student object extends an abstract Student object
//The constructor creates an Student object
public class PartTimeStudent<C, G, I, T, B> extends Student<C, G, I, T, B>{
    public PartTimeStudent(T name, I studentID, ArrayList<C> courseHistory, ArrayList<G> courseGradesHistory, B fullTime) {
        super.setName(name);
        super.setStudnetID(studentID);
        super.setCourseHistory(courseHistory);
        super.setCourseGradesHistory(courseGradesHistory);
        super.setIsFullTime(fullTime);
    }
}
