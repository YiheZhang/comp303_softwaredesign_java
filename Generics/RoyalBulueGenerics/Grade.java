/*
COMP 303 Assignment #3.2
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

//grade object
//this object contains a pair of an Course object and an Grade object
public class Grade<C, T> {
    private C course;
    private T grade;

    public Grade(C course, T grade) {
        this.course = course;
        this.grade = grade;
    }

    //methods to get private values
    public C getCourse() {
        return this.course;
    }
    public T getGrade() {
        return this.grade;
    }

    //methods to change private values
    public void setCourse(C course)
    {
        this.course = course;
    }
    public void setGrade(T grade) {
        this.grade = grade;
    }
}
