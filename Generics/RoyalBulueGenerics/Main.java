/*
COMP 303 Assignment #3.2
Date: 2018-10-01
Student name: Yihe Zhang
Student ID: 260738383
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {

        //initialize temp variables
        ArrayList<Student> students = new ArrayList<>();
        ArrayList<Course> courseHistory = new ArrayList<>();
        ArrayList<Grade> courseGradesHistory = new ArrayList<>();

        Student student = null;
        Course course = null;
        String grade = null;
        String name = null;
        String courseName = null;
        String courseNumber = null;
        Boolean isFullTime = true;
        int studentID = -1;

        //A loop for user i/o, let user select a number
        int endLoop = 1;
        while(endLoop == 0) {
            int selection = mainMenu();
            if (selection == 1) register(students, course);
            if (selection == 2) addGrade(student, course, grade);
            if (selection == 3) {
                Student newStudent = createAStudent(name, studentID, courseHistory, courseGradesHistory, isFullTime);
                students.add(newStudent);
            }
            if (selection == 4) {
                Course newCourse = createACourse(courseName, courseNumber);
            }
            if (selection == 5) endLoop = 1;
        }
    }

    //return an int, let user select an option.
    //(1) register students to a course, (2) add a grade to a student for a course
    //(3) create a student, (4) create a course, (5) quite program.
    private static <I> I mainMenu() {
        I selection = null;
        return selection;
    }

    //Option #1, input an array of Student, a Course
    private static <S, C> void register(ArrayList<S> students, C course) {
    }

    //Option #2, input a Student, a Course, a String for grade
    private static <S, C, T> void addGrade(S student, C course, T grade) {
    }

    //Option #3, input a String for name, an int for student ID, an array of Course for course History,
    //an array of Grade for course grades History. Return an Student object
    private static <S, I, C, G, T, B> S createAStudent(T name, I studentID, ArrayList<C> courseHistory, ArrayList<G> courseGradesHistory, B isFullTime) {
        S student = null;
        return student;
    }

    //Option #4, input a String for course name and course number, return an Course object
    private static <C, T> C createACourse(T courseName, T courseNumber) {
        C course = null;
        return course;
    }
}
