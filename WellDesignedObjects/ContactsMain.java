//File Name:    ContactsMain.java
//Developers:   Yihe Zhang
//Purpose:      User Interface
//Inputs:       Commands for creating, finding and listing contacts
//Outputs:      Contacts
//File version: 7
//======================================================================
//2018-9-20

import java.util.*;

public class ContactsMain {

    //create an array list to store names
    static ArrayList<Contacts> list = new ArrayList<Contacts>();
    private static int maxNameLength = 7, maxPhoneLength = 7, maxAddressLength = 7, maxBusinessNameLength = 17, maxBirthdateLength = 7;

    public static void main(String[] args) {
        boolean enterAgain = true;
        Scanner scanner = new Scanner(System.in);

        while (enterAgain){
            System.out.println("1. New Contact\n2. Find Contact\n3. List All\n4. Quit");
            System.out.print("Select: ");
            String method = scanner.nextLine();

            //method 1, new contact
            if (method.equals("1")) {
                newContact();
            }
            //method 2, find contact
            if (method.equals("2")) {
                findContact();
            }
            //method 3, list all
            if (method.equals("3")) {
                listAll();
            }
            //method 4, quit
            if (method.equals("4")) {
                enterAgain = false;
            }
        }
        scanner.close();
    }

    //method 1, create a new contact
    public static void newContact(){
        Scanner scanner = new Scanner(System.in);
        boolean enterAgain = true;

        while (enterAgain) {
            System.out.print("Enter Contact(Acquaintance, Business, or Friend): ");
            String contact = scanner.nextLine();

            if (contact.equals("Acquaintance")) {
                System.out.print("Enter Name: ");
                String name = scanner.nextLine();
                System.out.print("Enter Phone Number: ");
                String phone = scanner.nextLine();

                //Store the value to the array list
                Acquaintance acquaintance = new Acquaintance(name, phone);
                list.add(acquaintance);

                if (maxNameLength < name.length()) {
                    maxNameLength = name.length();
                }
                if (maxPhoneLength < phone.length()) {
                    maxPhoneLength = phone.length();
                }

                enterAgain = false;
            }
            if (contact.equals("Business")) {
                System.out.print("Enter Name: ");
                String name = scanner.nextLine();
                System.out.print("Enter Phone Number: ");
                String phone = scanner.nextLine();
                System.out.print("Enter Address: ");
                String address = scanner.nextLine();
                System.out.print("Enter Business name: ");
                String businessName = scanner.nextLine();

                //Store the value
                Business business = new Business(name, phone, address, businessName);
                list.add(business);

                if (maxNameLength < name.length()) {
                    maxNameLength = name.length();
                }
                if (maxPhoneLength < phone.length()) {
                    maxPhoneLength = phone.length();
                }
                if (maxAddressLength < address.length()) {
                    maxAddressLength = address.length();
                }
                if (maxBusinessNameLength < businessName.length()) {
                    maxBusinessNameLength = businessName.length();
                }

                enterAgain = false;
            }
            if (contact.equals("Friend")) {
                System.out.print("Enter Name: ");
                String name = scanner.nextLine();
                System.out.print("Enter Phone Number: ");
                String phone = scanner.nextLine();
                System.out.print("Enter Address: ");
                String address = scanner.nextLine();
                System.out.print("Enter Birthday: ");
                String birthday = scanner.nextLine();

                //Store the value
                Friend friend = new Friend(name, phone, address, birthday);
                list.add(friend);

                if (maxNameLength < name.length()) {
                    maxNameLength = name.length();
                }
                if (maxPhoneLength < phone.length()) {
                    maxPhoneLength = phone.length();
                }
                if (maxAddressLength < address.length()) {
                    maxAddressLength = address.length();
                }
                if (maxBirthdateLength < birthday.length()) {
                    maxBirthdateLength = birthday.length();
                }

                enterAgain = false;
            }
        }
    }

    //method 2, find Contact
    public static void findContact() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a name:");
        String name = scanner.nextLine();

        int i = 0, found = 0;
        while (i < list.size()) {
            if (list.get(i).getName().equals(name)) {
                printName(list.get(i));
                found = 1;
                System.out.println();
                break;
            }
            i++;
        }
        if (found == 0) {
            System.out.println("NOT FOUND");
        }
    }

    private static void printName(Contacts i){
        if (i.getType() == 1) {
            System.out.println("Type: Acquaintance");
            System.out.print("Name: "+i.getName()+"\n");
            System.out.print("Phone: "+i.getPhoneNumber());
        }
        else if (i.getType() == 2) {
            System.out.println("Type: Business");
            System.out.print("Name: "+i.getName()+"\n");
            System.out.print("Phone: "+i.getPhoneNumber()+"\n");
            System.out.print("Address: "+i.getAddress()+"\n");
            System.out.print("Business Name: "+i.getBusinessName());
        }
        else {
            System.out.println("Type: Friend");
            System.out.print("Name: "+i.getName()+"\n");
            System.out.print("Phone: "+i.getPhoneNumber()+"\n");
            System.out.print("Address: "+i.getAddress()+"\n");
            System.out.print("Birthday: "+i.getBirthday());
        }
    }

    //method 3, list all
    public static void listAll() {
        System.out.printf("%-5s%-"+(maxNameLength+5)+"s%-"+(maxPhoneLength+5)+"s%-"+(maxAddressLength+5)+"s%-"+(maxBusinessNameLength+5)+"s%-"+(maxBirthdateLength+5)+"s\n","TYPE","NAME","PHONE","ADDRESS","BUSINESS NAME","BIRTHDATE");
        for (int i=0; i<list.size(); i++){
            //Print Acquaintance
            if(list.get(i).getType()==1) {
                System.out.printf("%-5s%-"+(maxNameLength+5)+"s%-"+(maxPhoneLength+5)+"s%-"+(maxAddressLength+5)+"s%-"+(maxBusinessNameLength+5)+"s%-"+(maxBirthdateLength+5)+"s\n","A",list.get(i).getName(),list.get(i).getPhoneNumber(),"","","");
            }
            //Print Business
            if(list.get(i).getType()==2) {
                System.out.printf("%-5s%-"+(maxNameLength+5)+"s%-"+(maxPhoneLength+5)+"s%-"+(maxAddressLength+5)+"s%-"+(maxBusinessNameLength+5)+"s%-"+(maxBirthdateLength+5)+"s\n","B",list.get(i).getName(),list.get(i).getPhoneNumber(),list.get(i).getAddress(),list.get(i).getBusinessName(),"");
            }
            //Print Friend
            if(list.get(i).getType()==3) {
                System.out.printf("%-5s%-"+(maxNameLength+5)+"s%-"+(maxPhoneLength+5)+"s%-"+(maxAddressLength+5)+"s%-"+(maxBusinessNameLength+5)+"s%-"+(maxBirthdateLength+5)+"s\n","F",list.get(i).getName(),list.get(i).getPhoneNumber(),list.get(i).getAddress(),"",list.get(i).getBirthday());
            }
        }
    }
}