//File Name:    ContactTest.java
//Developers:   Yihe Zhang
//Purpose:      Contacts class
//Inputs:       name, phoneNumber, address, businessName, birthday
//Outputs:      a Acquaintance, Business, Friend object
//File version: 7
//======================================================================
//2018-9-20

abstract class Contacts {
    //type 1 = Acquaintance, 2 = Business, 3 = Friend
    private int type = -1;
    private String name, phoneNumber, address, businessName, birthday;

    public  void setType(int type) {
        this.type = type;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    //only the correct contact type can change its value
    public void setAddress(String address) {
        if (this.type == 2 || this.type ==3)
            this.address = address;
        else throw new IllegalArgumentException("Failed to set address for this type.");
    }
    public void setBusinessName(String businessName) {
        if (this.type == 2)
            this.businessName = businessName;
        else throw new IllegalArgumentException("Failed to set business name for this type.");
    }
    public void setBirthday(String birthday) {
        if (this.type == 3)
            this.birthday = birthday;
        else throw new IllegalArgumentException("Failed to set birthday for this type.");
    }

    public int getType() {
        return this.type;
    }
    public String getName() {
        return this.name;
    }
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    public String getAddress() {
        if (this.type == 2 || this.type == 3)
            return this.address;
        else throw new IllegalArgumentException("Failed to return address for this type.");
    }
    public String getBusinessName() {
        if (this.type == 2)
            return this.businessName;
        else throw new IllegalArgumentException("Failed to return business name for this type.");
    }
    public String getBirthday() {
        if (this.type == 3)
            return this.birthday;
        else throw new IllegalArgumentException("Failed to return birthday for this type.");
    }
}

class Acquaintance extends Contacts{
    public Acquaintance(String name, String phoneNumber) {
        super.setType(1);
        super.setName(name);
        super.setPhoneNumber(phoneNumber);
    }
}

class Business extends Contacts {
    public Business(String name, String phoneNumber, String address, String businessName) {
        super.setType(2);
        super.setName(name);
        super.setPhoneNumber(phoneNumber);
        super.setAddress(address);
        super.setBusinessName(businessName);
    }
}

class Friend extends Contacts {
    public Friend(String name, String phoneNumber, String address, String birthday) {
        super.setType(3);
        super.setName(name);
        super.setPhoneNumber(phoneNumber);
        super.setAddress(address);
        super.setBirthday(birthday);
    }
}
