//File Name:    ContactTest.java
//Developers:   Yihe Zhang
//Purpose:      Tester for contacts
//Inputs:       None
//Outputs:      Tested results
//File version: 7
//======================================================================
//2018-9-20

import java.util.*;
import java.util.stream.Collectors;

public class ContactsTest {
    public static void main(String[] args) {
        acquaintanceTest();
        businessTest();
        friendTest();
    }

    //test method for Acquaintance
    public static void acquaintanceTest() {

        for (int i=0; i<1; i++) {
            String tempName = randomString(5);
            String tempPhone = randomString(6);

            //initialize Acquaintance
            System.out.println("Test case for Acquaintance : Acquaintance ac = new Acquaintance("+tempName+", "+tempPhone+")");
            Acquaintance ac = new Acquaintance(tempName, tempPhone);

            try {
                System.out.print("Arguments: < ac.getName(); >   Result: ");
                System.out.println(ac.getName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(4);
                System.out.print("Arguments: < ac.setName("+temp+"); ac.getName(): >   Result: ");
                ac.setName(temp);
                System.out.println(ac.getName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < ac.getPhoneNumber(); >   Result: ");
                System.out.println(ac.getPhoneNumber());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(5);
                System.out.print("Arguments: < ac.setPhoneNumber("+temp+"); ac.getPhoneNumber(); >   Result: ");
                ac.setPhoneNumber(temp);
                System.out.println(ac.getPhoneNumber());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < ac.getAddress(); >   Result: ");
                System.out.println(ac.getAddress());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(6);
                System.out.print("Arguments: < ac.setAddress("+temp+"); ac.getAddress(); >   Result: ");
                ac.setAddress(temp);
                System.out.println(ac.getAddress());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < ac.getBusinessName(); >   Result: ");
                System.out.println(ac.getBusinessName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(7);
                System.out.print("Arguments: < ac.setBusinessName("+temp+"); ac.getBusinessName(); >   Result: ");
                ac.setBusinessName(temp);
                System.out.println(ac.getBusinessName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < ac.getBirthday(); >   Result: ");
                System.out.println(ac.getBirthday());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(8);
                System.out.print("Arguments: < ac.setBirthday("+temp+"); ac.getBirthday(); >   Result: ");
                ac.setBirthday(temp);
                System.out.println(ac.getBirthday());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            System.out.println();
        }
    }

    //test method for business
    public static void businessTest() {
        for (int i=0; i<1; i++) {
            String tempName = randomString(5);
            String tempPhone = randomString(6);
            String tempAddress = randomString(7);
            String tempBusinessName = randomString(8);

            //initialize Acquaintance
            System.out.println("Test case for Business : Business bu = new Business("+tempName+", "+tempPhone+", "+tempAddress+", "+tempBusinessName+")");
            Business bu = new Business(tempName, tempPhone, tempAddress, tempBusinessName);

            try {
                System.out.print("Arguments: < bu.getName(); >   Result: ");
                System.out.println(bu.getName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(4);
                System.out.print("Arguments: < bu.setName("+temp+"); bu.getName(): >   Result: ");
                bu.setName(temp);
                System.out.println(bu.getName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < bu.getPhoneNumber(); >   Result: ");
                System.out.println(bu.getPhoneNumber());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(5);
                System.out.print("Arguments: < bu.setPhoneNumber("+temp+"); bu.getPhoneNumber(); >   Result: ");
                bu.setPhoneNumber(temp);
                System.out.println(bu.getPhoneNumber());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < bu.getAddress(); >   Result: ");
                System.out.println(bu.getAddress());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(6);
                System.out.print("Arguments: < bu.setAddress("+temp+"); bu.getAddress(); >   Result: ");
                bu.setAddress(temp);
                System.out.println(bu.getAddress());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < bu.getBusinessName(); >   Result: ");
                System.out.println(bu.getBusinessName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(7);
                System.out.print("Arguments: < bu.setBusinessName("+temp+"); bu.getBusinessName(); >   Result: ");
                bu.setBusinessName(temp);
                System.out.println(bu.getBusinessName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < bu.getBirthday(); >   Result: ");
                System.out.println(bu.getBirthday());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(8);
                System.out.print("Arguments: < bu.setBirthday("+temp+"); bu.getBirthday(); >   Result: ");
                bu.setBirthday(temp);
                System.out.println(bu.getBirthday());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            System.out.println();
        }
    }

    //test method for friend
    public static void friendTest() {
        for (int i=0; i<1; i++) {
            String tempName = randomString(5);
            String tempPhone = randomString(6);
            String tempAddress = randomString(7);
            String tempBirthdate = randomString(9);

            //initialize Acquaintance
            System.out.println("Test case for Friend : Business fr = new Friend("+tempName+", "+tempPhone+", "+tempAddress+", "+tempBirthdate+")");
            Friend fr = new Friend(tempName, tempPhone, tempAddress, tempBirthdate);

            try {
                System.out.print("Arguments: < fr.getName(); >   Result: ");
                System.out.println(fr.getName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(4);
                System.out.print("Arguments: < fr.setName("+temp+");fr.getName(): >   Result: ");
                fr.setName(temp);
                System.out.println(fr.getName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: <fru.getPhoneNumber(); >   Result: ");
                System.out.println(fr.getPhoneNumber());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(5);
                System.out.print("Arguments: < fr.setPhoneNumber("+temp+"); fr.getPhoneNumber(); >   Result: ");
                fr.setPhoneNumber(temp);
                System.out.println(fr.getPhoneNumber());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < fr.getAddress(); >   Result: ");
                System.out.println(fr.getAddress());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(6);
                System.out.print("Arguments: < fr.setAddress("+temp+"); fr.getAddress(); >   Result: ");
                fr.setAddress(temp);
                System.out.println(fr.getAddress());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < fr.getBusinessName(); >   Result: ");
                System.out.println(fr.getBusinessName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(7);
                System.out.print("Arguments: < fr.setBusinessName("+temp+"); fr.getBusinessName(); >   Result: ");
                fr.setBusinessName(temp);
                System.out.println(fr.getBusinessName());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                System.out.print("Arguments: < fr.getBirthday(); >   Result: ");
                System.out.println(fr.getBirthday());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            try {
                String temp = randomString(8);
                System.out.print("Arguments: < fr.setBirthday("+temp+"); fr.getBirthday(); >   Result: ");
                fr.setBirthday(temp);
                System.out.println(fr.getBirthday());
            }
            catch (Exception e) {
                System.out.println(e);
            }

            System.out.println();
        }
    }

    //A method to generate a random |length| letter string
    public static String randomString(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        return new Random().ints(length, 0, chars.length()).mapToObj(i -> "" + chars.charAt(i)).collect(Collectors.joining());
    }
}