//A Strategy class that calculates values
public interface CalculationStrategy<T> {
    String getColumnOneHeader();
    String getColumnTwoHeader();
    String getItemName(T object);
    double getCalculatedValue(T object);
    String getUnitName();
}