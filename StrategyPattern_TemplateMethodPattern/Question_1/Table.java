import javax.swing.text.html.parser.Parser;
import java.util.ArrayList;
import java.util.Iterator;

public class Table<T> {
    private ArrayList<T> objects;
    private CalculationStrategy calculationStrategy;

    //Constructor, inputs an array of objects, name strategy and calculation strategy
    public Table (ArrayList<T> objects, CalculationStrategy calculationStrategy) {
        this.calculationStrategy = calculationStrategy;
        this.objects = objects;
    }

    //add an Item
    public void addItem(T item) {
        objects.add(item);
    }

    //remove an Item
    public void removeItem(T item) {
        objects.remove(item);
    }

    public void print() {

        System.out.printf("\n%8s %11s\n", calculationStrategy.getColumnOneHeader(), calculationStrategy.getColumnTwoHeader());
        System.out.println("--------------------");

        //prints the table
        Iterator i = objects.iterator();
        while (i.hasNext()) {
            Object temp = i.next();
            System.out.printf("%8s %11s\n", calculationStrategy.getItemName((temp)), Math.round(calculationStrategy.getCalculatedValue(temp))+" "+calculationStrategy.getUnitName());
        }
    }
}
