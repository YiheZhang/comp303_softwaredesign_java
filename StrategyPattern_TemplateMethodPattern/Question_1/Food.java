public class Food {
    private String name;
    private double carbs;
    private double fat;
    private double proteins;

    //Constructor
    public Food(String name, double carbs, double fat, double proteins) {
        this.name = name;
        this.carbs = carbs;
        this.fat = fat;
        this.proteins = proteins;
    }

    //Getters
    public String getName() {
        return name;
    }
    public double getCarbs() {
        return carbs;
    }
    public double getFat() {
        return fat;
    }
    public double getProteins() {
        return proteins;
    }
}
