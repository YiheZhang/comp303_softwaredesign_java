public class CalculateCals<T> implements CalculationStrategy<T> {
    @Override
    public String getColumnOneHeader() {
        return "Name";
    }
    @Override
    public String getColumnTwoHeader() {
        return "Calories";
    }
    @Override
    public String getItemName(T object) {
        return ((Food) object).getName();
    }
    @Override
    public double getCalculatedValue(T object) {
        Food food = (Food) object;
        return 4*food.getCarbs()+9*food.getFat()+4*food.getProteins();
    }
    @Override
    public String getUnitName() {
        return "Cal";
    }
}