import java.util.ArrayList;
import java.util.Iterator;

public class TableTM<T> {
    private ArrayList<T> objects;
    private CalculationTemplate calculationTemplate;

    //Constructor, inputs an array of objects, name strategy and calculation strategy
    public TableTM (ArrayList<T> objects, CalculationTemplate calculationTemplate) {
        this.objects = objects;
        this.calculationTemplate = calculationTemplate;
    }

    //add an Item
    public void addItem(T item) {
        objects.add(item);
    }

    //remove an Item
    public void removeItem(T item) {
        objects.remove(item);
    }

    public void print() {
        //Prints the header
        System.out.printf("\n%8s %11s\n",calculationTemplate.getColumnOneHeader(), calculationTemplate.getColumnTwoHeader());
        System.out.println("-------------------");

        //Prints the table
        Iterator i = objects.iterator();
        while (i.hasNext()) {
            Object temp = i.next();
            System.out.printf("%8s %11s\n", calculationTemplate.getItemName(temp), Math.round(calculationTemplate.getCalculatedValue(temp))+" "+calculationTemplate.getUnitName());
        }
    }
}
