public class CalculateCals<T> extends CalculationTemplate<T> {
    private Food food;
    private String name;
    private double carbs;
    private double fat;
    private double proteins;

    @Override
    public void setObject(T object){
        this.food = (Food) object;
    }
    //Initializes Value for the Food Object
    @Override
    public void initialize(){
        this.name = this.food.getName();
        this.carbs = this.food.getCarbs();
        this.fat = this.food.getFat();
        this.proteins = this.food.getProteins();
    }
    @Override
    public String getHeader1(){
        return "Name";
    }
    @Override
    public String getHeader2(){
        return "Calories";
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public String getUnit() {
        return "Cal";
    }
    @Override
    public double doCalculation(){
        return 4*carbs+9*fat+4*proteins;
    }
}
