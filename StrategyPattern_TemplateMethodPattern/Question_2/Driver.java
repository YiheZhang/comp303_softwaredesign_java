import java.util.ArrayList;

public class Driver {
    public static void main(String args[]) {
        //Initializes foods
        Food banana = new Food("banana", 27, 0, 1);
        Food egg = new Food("egg", 0, 5, 6);
        Food bagel = new Food("bagel", 56, 2, 11);

        //Adds foods to an array
        ArrayList<Food> foods = new ArrayList<>();
        foods.add(banana);
        foods.add(egg);

        //Initializes foods table
        TableTM foodsTable = new TableTM(foods, new CalculateCals());

        //Adds bagel to food Table
        foodsTable.addItem(bagel);
        foodsTable.print();


    }
}