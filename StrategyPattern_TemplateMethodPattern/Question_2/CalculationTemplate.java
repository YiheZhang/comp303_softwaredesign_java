public abstract class CalculationTemplate<T> {
    abstract void setObject(T object);
    abstract void initialize();
    //Gets the header of column 1 and 2
    abstract String getHeader1();
    abstract String getHeader2();
    //Gets the name of column 1 and value of column 2
    abstract String getName();
    abstract String getUnit();
    //method to calculate value
    abstract double doCalculation();

    //Template methods
    public final String getColumnOneHeader() {
        return getHeader1();
    }
    public final String getColumnTwoHeader() {
        return getHeader2();
    }
    public final String getItemName(T object) {
        setObject(object);
        initialize();
        return getName();
    }
    public final double getCalculatedValue(T object) {
        setObject(object);
        initialize();
        return doCalculation();
    }
    public final String getUnitName() {
        return getUnit();
    }
}
