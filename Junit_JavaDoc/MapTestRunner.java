import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * <h1>Map Test Runner</h1>
 * The Map Test Runner program calls methods from MapTest
 * using junit.
 *
 * @author  Yihe Zhang
 * @version 1.0
 * @since   October 2018
 */

public class MapTestRunner {
    /**
     * The main method is used to call other methods from MapTest class,
     * it will prints the results (success/failure).
     * @param args main method
     */
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(MapTest.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
} 