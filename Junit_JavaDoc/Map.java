/**
 * <h1>Map</h1>
 * The Map program implements an application that create
 * a map class from the user inputs, so that the created
 * map class can be shared with other developers.
 *
 * @author  Joseph Vybihal
 * @version 1.0
 * @since   September 2018
 */
public class Map {
    public static final int MAXINT = 2147483647;
    private char array[][];
    private int width, height;

    /**
     * This public static method can be used by external software
     * and it can validate their their input (Size of the map).
     * <b>
     * Map (array) cannot be created if param n is less or equal
     * to zero.
     * </b>
     *
     * @param n The input number must be greater than zero and less then max
     * @return boolean The validation result
     */
    public static boolean validSize(int n) {
        return n > 0;
    }

    /**
     * This public static method can be used by external software
     * and it can validate their their input (terrain symbol #/G/~).
     *
     * @param c The input number must be a terrain symbol (#/G/~)
     * @return boolean The validation result
     */
    public static boolean validTerrain(char c) {
        // A char array of all valid symbols and a loop could have also be used
        return (c == '~' || c == 'G' || c == '#');
    }


    /**
     * This constructor is used to build a safe world (Map array), and
     * throws error if the inputs are incorrect.
     * <p>
     * In order to do so, it calls other methods to valid parameters
     * width and and height.
     *
     * @param width The input number width (it must be greater than 0)
     * @param height The input number height (it must be greater than 0)
     */
    Map(int width, int height) throws Exception {
        int i, j;

        if (Map.validSize(width)) this.width = width;
        else throw new Exception("Incorrect width");

        if (Map.validSize(height)) this.height = height;
        else throw new Exception("Incorrect height");

        array = new char[width][height];

        for(i=0; i<height; i++)
            for(j=0;j<width;j++)
                array[j][i] = '~';
    }


    /**
     * This method is used to set terrain symbol on a map, and
     * throws error if the inputs are incorrect.
     * <p>
     * In order to do so, it calls other methods to valid parameters
     * row, col and terrain.
     *
     * @param row The input number width (it must be greater than 0)
     * @param col The input number col (it must be greater than 0)
     * @param terrain The input character terrain (it must be char #/G/~)
     * @throws Exception it throws an exception if the information is incorrect
     */
    public void setTerrain(int row, int col, char terrain) throws Exception {
        if ((validSize(row) || row==0) && row < height && (validSize(col) || col==0) && col < width && validTerrain(terrain))
            array[col][row] = terrain;
        else
            throw new Exception("Incorrect information");
    }

    /**
     * This public method is used to return a user created formatted string,
     * this string contains all the values in the map.
     *
     * @return String The formatted string
     */
    public String toString() {
        String result = "Terrain:\n\n";
        int i, j;

        for(i=0;i<height;i++) {
            for (j = 0; j < width; j++)
                result += " " + array[j][i];

            result += "\n";
        }

        return result;
    }
}
