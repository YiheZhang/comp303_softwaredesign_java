import junit.framework.*;
import org.junit.Test;

/**
 * <h1>Map Test</h1>
 * The Map Test program implements an application that
 * validates the Map.java class methods exhaustively
 *
 * @author  Yihe Zhang
 * @version 3.0
 * @since   October 2018
 */

public class MapTest extends TestCase{


    /**
     * This test method is used to validate ValidSize method.
     * <b>The expectation is that a valid size is greater than or equal to zero.</b>
     * <p>
     * Therefore testing -1, 0, and 1 is enough.
     */
    @Test
    public void testValidSize(){
        assertEquals(false, Map.validSize(-1));
        assertEquals(false, Map.validSize(0));
        assertEquals(true, Map.validSize(1));
    }

    /**
     * This test method is used to validate ValidTerrain method.
     * <b>The expectation is that a valid terrain is only these characters: ~ G #</b>
     * <p>
     * ValidTerrain method can only accept (~ G #).
     * If validTerrain can accept other character, this method will prompt error.
     * If validTerrain cannot accept terrain symbol, this method will also prompt error.
     */
    @Test
    public void testValidTerrain(){
        assertEquals(false, Map.validTerrain((char) 34));
        assertEquals(true, Map.validTerrain('#'));
        assertEquals(false, Map.validTerrain('$'));
        assertEquals(true, Map.validTerrain('G'));
        assertEquals(false, Map.validTerrain('H'));
        assertEquals(true, Map.validTerrain('~'));
        assertEquals(false, Map.validTerrain((char) 127));
    }

    /**
     * This test method is used to validate Constructor method.
     * <b>The expectation is that an array is created and no errors are thrown.</b>
     * <p>
     * This method tests if array is NULL and if error thrown.
     * This method tests negative boundary and too large integer boundary.
     */
    @Test
    public void testConstructor(){

        //Map map = new Map(-1,1);
        try {
            assertNotNull(new Map(-1, -1));
            fail("Map(-1, -1) must has error");
        }catch (Exception e) {
            /* This throw is expected. If thrown then good. No error message */
        }

        /* Height=-1 not valid */
        try {
            assertNotNull(new Map(5, -1));
            fail("Map(5, -1) must has error");
        } catch(Exception e) {
            /* This throw is expected. If thrown then good. No error message */
        }

        /* Height and Width too large not valid */
        try {
            assertNotNull(new Map(Map.MAXINT+1, Map.MAXINT+1));
            fail("Map(MAXINT+1, MAXINT+1) must has error");
        } catch(Exception e) {
            /* This throw is expected. If thrown then good. No error message */
        }

        try {
            assertNotNull(new Map(5, 5));
        } catch(Exception e) {
            /* if new Map(5, 5) has error, then fail */
            fail("for Map(5, 5): " + e);
        }
    }

    /**
     * This method will do a visual test in this case.
     * <b>The expectation is that the value stored in the location in the map is actually there.</b>
     * Assumes: testConstructor passed.
     * If the states are equal to the above states, fail()
     */
    @Test
    public void testSetTerrain() {

        /*
         *  Set -1, -1, G  == Exception
         *  Set  5, -1, G  == Exception
         *  Set -1,  5, G  == Exception
         *  Set  5,  5, G  == Success
         *  Set  5,  6, T  == Exception
         *  Set  0,  0, G  == Success
         *  Set  9,  9, G  == Success
         *  Set  5,  6, #  == Success
         *  Set  5,  7, ~  == Success
         */

        /* Has thrown */
        try {
            new Map(10, 10).setTerrain(-1,-1,'G');
            fail("setTerrain(-1,-1,'G') must have exception");
        }
        catch(Exception e){ }

        /* Has thrown */
        try {
            new Map(10, 10).setTerrain(5,-1,'G');
            fail("setTerrain(5,-1,'G') must have exception");
        }
        catch(Exception e){ }

        /* Has thrown */
        try {
            new Map(10, 10).setTerrain(-1,5,'G');
            fail("setTerrain(-1,5,'G') must have exception");
        }
        catch(Exception e){ }

        /* No thrown */
        try {
            new Map(10, 10).setTerrain(5,5,'G');
        }
        catch(Exception e){
            fail("setTerrain(5,5,'G') cannot have exception");
        }

        /* Has thrown */
        try {
            new Map(10, 10).setTerrain(5,6,'T');
            fail("setTerrain(5,6,'T') must have exception");
        }
        catch(Exception e){ }

        /* No thrown */
        try {
            new Map(10, 10).setTerrain(0,0,'G');
        }
        catch(Exception e) {
            fail("setTerrain(0,0,'G') cannot have exception");
        }

        /* No thrown */
        try {
            new Map(10, 10).setTerrain(9,9,'G');
        }
        catch(Exception e){
            fail("setTerrain(9,9,'G') cannot have exception");
        }

        /* No thrown */
        try {
            new Map(10, 10).setTerrain(5,6,'#');
        }
        catch(Exception e){
            fail("setTerrain(5,6,'#') cannot have exception");
        }

        /* No thrown */
        try {
            new Map(10, 10).setTerrain(5,7,'~');
        }
        catch(Exception e){
            fail("setTerrain(5,7,'~') cannot have exception");
        }
    }

    /**
     * No special testing, expecting to see all water.
     * <b>The expectation is that the full string displays.</b>
     * <p>
     * Assumes: test set terrain passed.
     */
    public void testToString() {
        try {
            /* We should see 4 water cells in 2 by 2 */
            assertEquals("Terrain:\n\n ~ ~\n ~ ~\n", new Map(2, 2).toString());
        }
        catch (Exception e){
            fail("ToString method failed");
        }

    }
}