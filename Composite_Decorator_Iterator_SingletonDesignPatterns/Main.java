public class Main {
    public static void main(String[] args) {
        try {
            Menu menu = new Menu();
            OrderingSystem a = new OrderingSystem(menu);

            //display menu
            a.displayMenu();

            //create 3 orders
            a.startOrder("1");
            a.startOrder("2");
            a.startOrder("3");

            //add 4 items to order #2
            a.addItem("2", "Side#2");
            a.addItem("2", "Dessert#1");
            a.addItem("2", "Drink#1");
            a.addItem("2", "Appetizer#2");
            a.addItem("2", "Side#2");

            //add alcohol drink and create a Meal to order #3
            a.addItem("1", "AlcoholDrink#1");
            a.addMeal("1", "Appetizer#1", "MainCourse#1", "Dessert#2", "FullMeal");
            a.addMeal("1", "Appetizer#2", "MainCourse#2", "Dessert#1", "FullMeal2");

            //display order #123
            a.displayOrder("2");
            a.displayOrder("1");
            a.displayOrder("3");

            //pay and close order #1
            a.pay("1", 1100);
            a.close("1");
            a.pay("1", 322.5);
            a.close("1");

            //close order #3
            a.close("3");
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
