import java.rmi.NotBoundException;

//an abstract class for Item
public abstract class Item {
    private double price;
    private String name;
    private int type;

    //Constructor
    public Item(String name, double price, int type) {
        this.name = name;
        this.price = price;
        this.type =type;
    }
    public Item(String name) {
        this.name = name;
    }
    //Basic functions
    public String getName() {
        return name;
    }
    public double getPrice() {
        return price;
    }
    public int getType() {
        return type;
    }
    public boolean isAlcoholized() {
        throw new UnsupportedOperationException();
    }
}

