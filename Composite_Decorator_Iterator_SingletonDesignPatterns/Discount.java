//Decorator for Item object
//Discount type == regular item type + 5
public class Discount extends Item{
    private double regularPrice;
    private boolean alcoholized;

    //Constructor for Item
    public Discount(Item x, double percentage) {
        super(x.getName(), percentage * x.getPrice(),x.getType()+5);
        this.regularPrice = x.getPrice();
    }

    //Constructor for Drink
    public Discount(Item x, double percentage, boolean alcoholized) {
        super(x.getName(), percentage * x.getPrice(),x.getType()+5);
        this.regularPrice = x.getPrice();
        this.alcoholized = alcoholized;
    }

    public double getRegularPrice() { return this.regularPrice; }
    @Override
    public boolean isAlcoholized() { return this.alcoholized; }
}
