import java.util.ArrayList;
import java.util.Iterator;

//this class is an Order decorator
public class OrderingSystem extends Item{
    private Menu menu;
    private ArrayList<Item> orders;

    //initializes menu
    public OrderingSystem(Menu menu) {
        super("OrderingSystem");
        orders = new ArrayList<>();
        this.menu = menu;
    }

    //creates an order, adds that order to the order list
    public void startOrder(String orderNumber) {
        Item a = new Order(orderNumber);
        orders.add(a);
    }

    //displays all items in one order
    public void displayOrder(String orderNumber) throws Exception{
        //gets order index from the list
        int index = getOrderIndex(orderNumber);
        Item order = orders.get(index);
        //calls the function
        ((Order) order).displayOrders();
    }

    public void addItem(String orderNumber, String name) throws Exception{
        //Find the specific Order
        int index = getOrderIndex(orderNumber);
        Item order = orders.get(index);
        orders.remove(index);
        System.out.println("Adding " + name + " to Order#" + orderNumber);

        //adds item to the specific order
        Item item = menu.getItem(name);

        //check if the item is today's special
        if (item.getName().equals(menu.getSpecial().getName())) {
            item = menu.getSpecial();
        }

        //If adds Alcohol Drink to that order
        if (item.getType() == 5 || item.getType() == 10) {
            System.out.println("WARING: Customer must be at least 18 years old");
        }
        ((Order) order).addItem(item);
        orders.add(order);
        System.out.println();
    }

    public void addMeal(String orderNumber, String appetizer, String mainCourse, String dessert, String mealName) throws Exception{
        //Find the specific Order
        int index = getOrderIndex(orderNumber);
        Item order = orders.get(index);
        orders.remove(index);
        System.out.println("Adding " + mealName + " to Order#" + orderNumber);

        //gets specific item from the menu
        Item app = menu.getItem(appetizer);
        Item main = menu.getItem(mainCourse);
        Item dess = menu.getItem(dessert);

        if (app.getType() != 1 || main.getType() !=2 || dess.getType() !=3) {
            throw new Exception("Invalid Items for Meal");
        }
        else {
            ((Order) order).addItem(new Meal(app, main, dess, mealName));
            orders.add(order);
            System.out.println();
        }
    }

    //a function find order index
    public int getOrderIndex(String orderNubmer) throws Exception{
        Iterator listIterator = orders.iterator();
        int index = 0;
        boolean found = false;
        while(listIterator.hasNext()) {
            Order order = (Order) listIterator.next();
            if (order.getOrderNumber().equals(orderNubmer)) {
                found = true;
                break;
            }
            index++;
        }
        if (!found) throw new Exception("Order Not Found");
        return index;
    }

    //display menu
    public void displayMenu() {
        menu.display();
    }

    //Pay balance
    public void pay(String orderNumber, double amount) throws Exception {
        int index = getOrderIndex(orderNumber);
        Item order = orders.get(index);
        ((Order) order).payBalance(amount);
        System.out.println ("Order #" + orderNumber + " --- Your Current Balance is $" + ((Order) order).getPrice() + "\n");
    }

    //removes an order from the list
    public void close(String orderNumber) throws Exception {
        int index = getOrderIndex(orderNumber);
        Item order = orders.get(index);

        //Removes Order Item from the list only if the balance is less then zero
        if (order.getPrice() <= 0) {
            orders.remove(index);
            System.out.println ("Order #" + orderNumber + " is closed\n");
        }
        else {
            System.out.println ("Unable to close Order #" + orderNumber +", Your balance must be <= zero");
            System.out.println ("Your Current Balance $" + order.getPrice() + "\n");
        }
    }
}