import java.util.ArrayList;
import java.util.Iterator;

//this class is an item composite
public class Order extends Item{
    private ArrayList<Item> list;
    private String orderNumber;
    private double total, balance;

    //constructor
    public Order(String orderNumber) {
        super(orderNumber);
        list = new ArrayList<Item>();
        this.orderNumber = orderNumber;
        this.total = 0;
    }

    //gets this order's number
    public String getOrderNumber(){
        return this.orderNumber;
    }

    //return balance instead full price
    @Override
    public double getPrice() {
        return this.balance;
    }

    public void payBalance(double amount) {
        balance -= amount;
    }

    //adds an item to the list
    public void addItem(Item x) {
        this.balance += x.getPrice();
        this.list.add(x);
    }

    //removes an item to the list
    public void removeItem(Item x) {
        this.list.remove(x);
        this.balance -= x.getPrice();
    }

    //displays current order
    public void displayOrders() {
        System.out.println("Order #" + this.orderNumber);
        Iterator listIterator = this.list.iterator();
        while(listIterator.hasNext()) {
            Item temp = (Item) listIterator.next();
            //Displays Discount Item
            if (temp.getType() > 5 && temp.getType() < 11) {
                Discount temp2 = (Discount) temp;
                System.out.println(temp2.getName() + " --- Discounted Price $" + temp2.getPrice() + " - Regular Price $" + temp2.getRegularPrice());
            }
            //Displays Meal
            else if (temp.getType() == 11) {
                Meal temp2 = (Meal) temp;
                System.out.println(temp2.getFullName() + " --- Discounted Price $" + temp2.getPrice() + " - Regular Price $" + temp2.getRegularPrice());
            }
            //Displays Regular Item
            else {
                System.out.println(temp.getName() + " --- Regular Price $" + temp.getPrice());
            }
            total += temp.getPrice();
        }
        System.out.println("Total --- $" + total + "\n");
    }
}
