import java.util.ArrayList;
import java.util.Iterator;

//Item Composite
public class Menu extends Item {
    private ArrayList<Item> list;
    private Item special;

    public Menu() {
        super("menu");
        list = new ArrayList<Item>();

        //default menu list
        add(new Appetizer("Appetizer#1", 50));
        add(new Appetizer("Appetizer#2", 30));
        add(new MainCourse("MainCourse#1", 800));
        add(new MainCourse("MainCourse#2", 500));
        add(new Desert("Dessert#1", 30));
        add(new Desert("Dessert#2", 15));
        add(new Side("Side#1", 30));
        add(new Side("Side#2", 20));
        add(new Drink("AlcoholDrink#1", 20, true));
        add(new Drink("Drink#1", 10, false));

        //default today's special
        setItemOfTheDay("Side#2", 0.7);
    }

    //adds an item to menu
    public void add(Item x) {
        list.add(x);
    }

    public void setItemOfTheDay(String name, double discount) {
        try {
            Item temp = getItem(name);
            //check if special item is alcohol Drink
            if (temp.getType() == 5) {
                special = new Discount(temp, discount, temp.isAlcoholized());
            } else {
                special = new Discount(temp, discount);
            }
        }
        catch (Exception e) {
            //System.out.println(e);
        }
    }

    //This is a function to create default Meal in the menu
    public void createMeal(String appetizer, String mainCourse, String desert, String mealName) {
        try {
            list.add(new Meal(getItem(appetizer), getItem(mainCourse), getItem(desert), mealName));
        }
        catch (Exception e) {
            //System.out.println(e);
        }
    }

    //removes an item from menu
    public void remove(String name) throws Exception {
        Iterator listIterator = list.iterator();
        Item temp = null;
        boolean found = false;

        while(listIterator.hasNext()) {
            temp = (Item) listIterator.next();
            if (temp.getName().equals(name)) {
                found = true;
                list.remove(temp);
                break;
            }
        }
        //if item does not in the list, throw error
        if (!found) { throw new Exception("Item Not found"); }
    }
    //Searches for items from its name
    public Item getItem(String name) throws Exception {
        Iterator listIterator = list.iterator();
        Item temp = null;
        boolean found = false;

        while(listIterator.hasNext()) {
            temp = (Item) listIterator.next();
            if (temp.getName().equals(name)) {
                found = true;
                break;
            }
        }
        if (found) { return temp; }
        else { throw new Exception("Item Not found"); }
    }
    public Item getSpecial() {
        return this.special;
    }

    //Displays available items stored in menu
    public void display() {
        System.out.println("Menu");
        Iterator listIterator = list.iterator();
        while(listIterator.hasNext()) {
            Item temp = (Item) listIterator.next();
            //check if the current item is Meal
            if (temp.getType() == 11) {
                Meal temp2 = (Meal) temp;
                System.out.println(temp2.getFullName() + " --- Discounted Price $" + temp2.getPrice() + " - Regular Price $" + temp2.getRegularPrice());
            }
            else
                System.out.println(temp.getName() + " --- Regular Price $" + temp.getPrice());
        }

        System.out.println("\nItem of the day");
        Discount temp = (Discount) this.special;
        System.out.println(this.special.getName() + " --- Discounted Price $" + this.special.getPrice() + " - Regular Price $" + temp.getRegularPrice());
        System.out.println("\n");
    }
}

//Item Leaf
//type 1 = appetizer
//type 2 = main course
//type 3 = dessert
//type 4 = side
//type 5 = drink
class Appetizer extends Item {
    Appetizer(String name, double price) {
        super(name, price, 1);
    }
}
class MainCourse extends Item {
    MainCourse(String name, double price) {
        super(name, price, 2);
    }
}
class Desert extends Item {
    Desert(String name, double price) {
        super(name, price, 3);
    }
}
class Side extends Item {
    Side(String name, double price) {
        super(name, price, 4);
    }
}
class Drink extends Item {
    private boolean alcoholized;
    Drink(String name, double price, boolean alcoholized) {
        super(name, price,5);
    }
    @Override
    public boolean isAlcoholized() { return this.alcoholized; }
}