public class Meal extends Item {
    private Item appetizer, mainCourse, desert;
    private String mealName;

    //needs 4 inputs
    //mealName is the name stored in Arraylist
    public Meal(Item appetizer, Item mainCourse, Item desert, String mealName) {
        super(mealName, appetizer.getPrice() + mainCourse.getPrice() + (new Discount(desert, 0.5)).getPrice(), 11);
        this.appetizer = appetizer;
        this.mainCourse = mainCourse;
        this.desert = new Discount(desert, 0.5);
        this.mealName = mealName;
    }
    //This is the name for display
    public String getFullName() {
        return mealName  + " - (" + appetizer.getName() + "+"
                + mainCourse.getName() + "+" + desert.getName() + ")";
    }

    //return regular price
    public double getRegularPrice() {
        Discount temp = (Discount) this.desert;
        return this.appetizer.getPrice() + this.mainCourse.getPrice() + temp.getRegularPrice();
    }
}
