//File Name:    Map.java
//Developers:   Yihe Zhang
//Purpose:      Create a map
//Inputs:       Row, width and object
//Outputs:      None
//File version: 6
//======================================================================
//2018-9-13

public class Map {
    private int maxRow, maxWidth;
    private char[][] map;

    //Constructor
    public Map(int row, int column) {
        //Throws an exception if the input is not valid
        if(row<=0 || row>Integer.MAX_VALUE) {
            throw new IllegalArgumentException("A row must be greater than 0.");
        }
        else if(column<=0 || column>Integer.MAX_VALUE) {
            throw new IllegalArgumentException("A column must be greater than 0.");
        }
        else {
            maxRow = row;
            maxWidth = column;
            this.map = new char[maxRow][maxWidth];

            //initializes the map with the water character
            for (int i = 0; i < maxRow; i++) {
                for (int j = 0; j < maxWidth; j++) {
                    map[i][j] = '~';
                }
            }
            System.out.print("Map has been created. ");
        }
    }

    public int getMaxRow() {
        return maxRow;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    //adds an object to a specific slot of the map
    public void addObject(int row, int column, char object) {

        if (object!='~'&&object!='G'&&object!='#') {
            throw new IllegalArgumentException("Invalid character! It must be either ~ or G or #.");
        }
        map[row][column] = object;
        System.out.println("Your "+object+" is added to "+row+", "+column+" in the map");
    }

    //prints the matrix
    public void print() {
        for(int i=0;i<maxRow;i++) {
            for(int j=0;j<maxWidth;j++) {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }

    //changes the value of maxRow
    public void setMaxRow(int row) {
        if(row<=0) {
            throw new IllegalArgumentException("A row must be greater than 0.");
        }
        else {
            maxRow = row;
            System.out.println("A new max row is set");
        }

    }

    //changes the value of maxWidth
    public void setMaxWidth(int column) {
        if(column<=0) {
            throw new IllegalArgumentException("A column must be greater than 0.");
        }
        else {
            maxWidth = column;
            System.out.println("A new max column is set");
        }
    }
}