//File Name:    MapMain.java
//Developers:   Yihe Zhang
//Purpose:      User interface
//Inputs:       Row, width and object
//Outputs:      Matrix
//File version: 6
//======================================================================
//2018-9-13

import java.util.Scanner;

public class MapMain {
    public static void main(String[] args) {
        System.out.println("Welcome to Map!");
        System.out.print("Please input the maximum number of rows: ");

        //reads rows
        Scanner scanner = new Scanner(System.in);
        while(!scanner.hasNextInt()){
            System.out.println("Input must be an integer.");
            System.out.print("Please input the maximum number of rows: ");
            scanner.next();
        }
        int inputRow = scanner.nextInt();
        while (inputRow <= 0) {
            System.out.println("A row must be greater than 0.");
            System.out.print("Please input the maximum number of rows: ");
            inputRow = scanner.nextInt();
        }

        //reads columns
        System.out.print("Please input the maximum number of columns: ");
        while(!scanner.hasNextInt()){
            System.out.println("Input must be an integer.");
            System.out.print("Please input the maximum number of columns: ");
            scanner.next();
        }
        int inputWidth = scanner.nextInt();
        while (inputWidth<=0) {
            System.out.println("A columns must be greater than 0.");
            System.out.print("Please input the maximum number of columns: ");
            inputWidth = scanner.nextInt();
        }

        //constructs the matrix
        Map map = new Map(inputRow, inputWidth);
        System.out.println();

        //A variable to check if adding an object again is necessary
        boolean addAgain = true;
        while(addAgain) {

            System.out.println("Please add an object to the map (~ for water, G for grass, # for tree)");

            //reads object row
            System.out.print("Row: ");
            while(!scanner.hasNextInt()){
                System.out.println("Input must be an integer.");
                System.out.print("Row: ");
                scanner.next();
            }
            inputRow = scanner.nextInt();
            while (inputRow<0||inputRow>=map.getMaxRow()){
                System.out.println("Invalid row! It must be between 0 and "+(map.getMaxRow()-1)+".");
                System.out.print("Row: ");
                inputRow = scanner.nextInt();
            }

            //reads object column
            System.out.print("Column: ");
            while(!scanner.hasNextInt()){
                System.out.println("Input must be an integer.");
                System.out.print("Column: ");
                scanner.next();
            }
            inputWidth = scanner.nextInt();
            while (inputWidth<0||inputWidth>=map.getMaxWidth()){
                System.out.println("Invalid column! It must be between 0 and "+(inputWidth-1)+".");
                System.out.print("Column: ");
                inputWidth = scanner.nextInt();
            }

            //reads an object
            System.out.print("Character: ");
            char inputObject = scanner.next().charAt(0);
            while (inputObject!='~'&&inputObject!='G'&&inputObject!='#'){
                System.out.println("Invalid character! It must be either ~ or G or #.");
                System.out.print("Character: ");
                inputObject = scanner.next().charAt(0);
            }

            //calls addObject method
            map.addObject(inputRow, inputWidth, inputObject);

            System.out.print("Would you like to enter another character (Y/N): ");
            char yesOrNo = scanner.next().charAt(0);

            while(yesOrNo!='Y'&&yesOrNo!='N'){
                System.out.println("Invalid character! It must be either Y or N.");
                System.out.print("Would you like to enter another character (Y/N): ");
                yesOrNo = scanner.next().charAt(0);
            }

            //if add again == false, break the while loop.
            if (yesOrNo=='N'){
                addAgain = false;
            }
        }
        //closes the scanner
        scanner.close();

        //prints the matrix
        map.print();
    }
}
