//File Name:    MapTest.java
//Developers:   Yihe Zhang
//Purpose:      Test all methods from Map.java
//Inputs:       Row, width and object
//Outputs:      Test results
//File version: 6
//======================================================================
//2018-9-13

public class MapTest {
    private static Map map;

    public static void main(String[] args) {
        //Boundary [-5, 15] Starts a loop, counting number from -5 to 15
        mapConstructorTest(-5, 15);
        getMaxRowTest(-1,3);
        getMaxWidthTest(-5,15);

        //creates a new map with max row 3, and max width 3, adds objects '~', '#', 'G' and 'S'
        addObjectTest(3, 3, 'S');

        //Boundary [-5, 11]
        setMaxRowTest(-5, 11);
        setMaxWidthTest(-5, 11);

    }

    public static void mapConstructorTest(int start, int stop) {

        System.out.print("\nTest case for method <<Map Constructor;>>");

        //constructs the map for (stop - start) times
        for (int row=start; row<=stop; row++) {
            for (int column=start; column<=stop; column++) {
                System.out.print("\nArguments: <<Map map = new Map("+row+", "+column+");>> ");
                System.out.print("Result: ");
                try {
                    map = new Map(row, column);
                }
                catch (Exception e) {
                    System.out.print(e);
                }
            }
        }

        System.out.println();
    }

    public static void getMaxRowTest(int start, int stop) {
        System.out.println("\nTest case for method <<getMaxRow();>>");

        for (int row=start; row<=stop; row++) {
            for (int column=start; column<=stop; column++) {

                boolean hasException = false;

                System.out.print("Arguments: <<Map map = new Map("+row+", "+column+"); getMaxRow;>> ");
                System.out.print("Result: ");
                try {
                    map = new Map(row, column);
                }
                catch (Exception e){
                    System.out.print(e+"\n");
                    hasException = true;
                }

                //if maxRow == input row, valid, else not valid
                if (!hasException) {
                    //gets max row
                    int maxRow = map.getMaxRow();
                    if(maxRow==row){
                        System.out.println("returned max row=" + maxRow + " Valid");
                    }
                    else {
                        System.out.println("Not Valid");
                    }
                }
            }
        }
    }

    public static void getMaxWidthTest(int start, int stop) {
        System.out.println("\nTest case for method <<getMaxWidth();>>");

        for (int row=start; row<=stop; row++) {
            for (int column=start; column<=stop; column++) {

                boolean hasException = false;

                System.out.print("Arguments: <<Map map = new Map("+row+", "+column+"); getMaxWidth;>> ");
                System.out.print("Result: ");
                try {
                    map = new Map(row, column);
                }
                catch (Exception e) {
                    System.out.print(e+"\n");
                    hasException = true;
                }

                //if maxWidth == input row, valid, else not valid
                if (!hasException) {
                    //gets max width
                    int maxWidth = map.getMaxWidth();
                    if(maxWidth==column) {
                        System.out.println("returned max width="+maxWidth+" Valid");
                    }
                    else {
                        System.out.println("Not Valid");
                    }
                }
            }
        }
    }

    public static void addObjectTest(int maxRow, int maxWidth, char symbol) {

        System.out.println("\nTest case for method <<addObject();>>");
        //constructs a maxRox*maxWidth matrix
        try {
            map = new Map(maxRow, maxWidth);
            System.out.println();
        }
        catch (Exception e) {
            System.out.print("Arguments: <<Map map = new Map("+maxRow+", "+maxWidth+");>> ");
            System.out.print("Result: ");
            System.out.println(e);
        }
        //tries to add 4 objects to every slot in that matrix
        for (int i=0; i<=maxRow; i++) {
            for (int j=0; j<=maxWidth; j++) {

                //test char ~
                System.out.print("Arguments: <<Map map = new Map(" + maxRow + ", " + maxWidth + "); addObject(" + i + ", " + j + ", '~');>> ");
                System.out.print("Result: ");
                try {
                    map.addObject(i, j, '~');
                }
                catch (Exception e) {
                    System.out.println(e);
                }

                //test char #
                System.out.print("Arguments: <<Map map = new Map(" + maxRow + ", " + maxWidth + "); addObject(" + i + ", " + j + ", '#');>> ");
                System.out.print("Result: ");
                try {
                    map.addObject(i, j, '#');
                }
                catch (Exception e) {
                    System.out.println(e);
                }

                //test char G
                System.out.print("Arguments: <<Map map = new Map(" + maxRow + ", " + maxWidth + "); addObject(" + i + ", " + j + ", 'G');>> ");
                System.out.print("Result: ");
                try {
                    map.addObject(i, j, 'G');
                }
                catch (Exception e) {
                    System.out.println(e);
                }

                //test the char inputted
                System.out.print("Arguments: <<Map map = new Map(" + maxRow + ", " + maxWidth + "); addObject(" + i + ", " + j + ", '"+symbol+"');>> ");
                System.out.print("Result: ");
                try {
                    map.addObject(i, j, symbol);
                }
                catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
    }

    public static void setMaxRowTest(int start, int stop) {
        System.out.println("\nTest case for method <<setMaxRowTest();>>");

        //constructs a 10*10 matrix
        try {
            map = new Map(10, 10);
            System.out.println();
        }
        catch (Exception e) {
            System.out.print("Arguments: <<Map map = new Map(10, 10);>> ");
            System.out.print("Result: ");
            System.out.println(e);
        }

        //tests that matrix
        for (int i=start; i<=stop; i++) {
            try {
                System.out.print("Arguments: <<Map map = new Map(10, 10); setMaxRowTest("+i+");>> ");
                System.out.print("Result: ");
                map.setMaxRow(i);
            }
            catch (Exception e){
                System.out.println(e);
            }
        }
    }

    public static void setMaxWidthTest(int start, int stop) {
        System.out.println("\nTest case for method <<setMaxWidthTest();>>");

        //constructs a 5*5 matrix
        try {
            map = new Map(5, 5);
            System.out.println();
        }
        catch (Exception e) {
            System.out.print("Arguments: <<Map map = new Map(5, 5);>> ");
            System.out.print("Result: ");
            System.out.println(e);
        }

        //tests that matrix
        for (int i=start; i<=stop; i++) {
            try {
                System.out.print("Arguments: <<Map map = new Map(5, 5); setMaxWidthTest("+i+");>> ");
                System.out.print("Result: ");
                map.setMaxWidth(i);
            }
            catch (Exception e){
                System.out.println(e);
            }
        }
    }

}