import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Controller extends KeyAdapter {
    private GameModel gameModel;
    private GamePanel gamePanel;

    public Controller(GameModel gameModel,GamePanel gamePanel) {
        this.gameModel = gameModel;
        this.gamePanel = gamePanel;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            gameModel.resetGame();
        }
        if (!gameModel.canMove()) {
            gameModel.setMyLose(true);
        }

        if (!gameModel.getMyWin() && !gameModel.getMyLose()) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_LEFT:
                    gameModel.left();
                    break;
                case KeyEvent.VK_RIGHT:
                    gameModel.right();
                    break;
                case KeyEvent.VK_DOWN:
                    gameModel.down();
                    break;
                case KeyEvent.VK_UP:
                    gameModel.up();
                    break;
            }
        }

        if (!gameModel.getMyWin() && !gameModel.canMove()) {
            gameModel.setMyLose(true);
        }

        gamePanel.repaint();
    }
}

