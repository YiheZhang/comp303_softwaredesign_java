import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        JFrame gameFrame = new JFrame();
        gameFrame.setTitle("2048 Game");
        gameFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        gameFrame.setSize(340, 400);
        gameFrame.setResizable(true);

        GameModel gameModel = new GameModel();
        GamePanel gamePanel = new GamePanel(gameModel);
        gameFrame.add(gamePanel);
        gamePanel.addKeyListener(new Controller(gameModel,gamePanel));

        gameFrame.setLocationRelativeTo(null);
        gameFrame.setVisible(true);
    }
}
